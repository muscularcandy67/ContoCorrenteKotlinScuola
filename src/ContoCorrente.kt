class ContoCorrente (var conto:Double) {

   init {
       setConto(conto)
   }

    fun setConto(c:Double):Boolean{
        if(c>=0){
            this.conto=c
            return true
        }
        return false
    }

    fun preleva(pr:Double):Double{
        if(pr<0) return -2.0
        if(pr>conto) return -1.0
        return conto-pr
    }

    fun accredita(accredito: Double): Double {
        return if (accredito < 0) -2.0
        else{
            conto += accredito
            conto
        }
    }

    fun visualizzaSaldo(): Double {
        return conto
    }

}