
import java.util.Random

class ContoCorrenteEvoluto (conto:Double) {


    private var password: String? = null
    private val conto: ContoCorrente = ContoCorrente(conto)

    fun makePassword(): String? {
        val p = CharArray(6)
        val r = Random()
        for (i in 0..5) {
            p[i] = Char::class.javaPrimitiveType!!.cast(r.nextInt())
        }
        password = p.toString()
        return password
    }

    fun controlloPassword(pw: String): Boolean {
        return password == pw
    }

    fun changePassword(pwAct: String, pwNew: String): Boolean {
        if (pwAct == password) {
            if (pwNew.length == 6) {
                password = pwNew
                return true
            }
        }
        return false
    }

}